# Cooler Chat  

Cooler Chat is a simple decentralized instant messaging application.  
Users can create a chat room and others will be able to join that chat room to share text based messages.  

[Download here](https://gitlab.com/a01200356/CoolerChat/blob/master/CoolerChat.jar)  

## Instructions  
### How to run  
 * Make sure a Java Virtual Machine is installed on your computer. If not, you can download it [here](https://www.java.com) or from your operating system's package manager.  
 * Go to your firewall settings to ensure that port **1789** is open for both incoming and outgoing traffic. The specific instructions depend on your system's firewall.  
 * Download the CoolerChat.jar file [here](https://gitlab.com/a01200356/CoolerChat/blob/master/CoolerChat.jar).  
 * Execute `java -jar CoolerChat.jar` in your command line.  
 * If you want to join an existing chat room you will need to know the IP address of one running peer of that chat room and write it in the `Remote peer's IP address` field.  
 * If you want to create a new chat room, leave the `Remote peer's IP address` field blank (just press Enter) and share your IP address with other people.  
 * You will be automatically notified of peers that enter or exit the chat in real time.  
 * To send a message, type a line of text and press Enter. The message will be sent to all currently available peers.
 * Type `ADIOS` to quit Cooler Chat.

### Troubleshooting  
If you cannot connect to a remote peer:  
 * Make sure you can connect to the Internet.  
 * Make sure your computer's firewall allows **both incoming and outgoing traffic** in port 1789.  
 * Make sure that your local network's firewall allows both incoming and outgoing traffic in port 1789. If you're in a home network, enter your router's firewall configurations to open port 1789. If you are in a public network, it's possible that you may not be able to use Cooler Chat.  

### Running Cooler Chat over a LAN  
You can also use Cooler Chat over a LAN by using the private IP address of a peer to find it. This assumes that all the peers are located on the same LAN. This might come handy for using Cooler Chat with people who are nearby, even if the public network is blocking ports.  

## Privacy notice  
Cooler Chat is **NOT** meant to provide secure communications.  
Any chat room created through CoolerChat is public and open to anyone that knows the IP address of any active peer.  
Additionally, messages between peers are transmitted in plain text (i.e. unencrypted).  
Use at your own discretion.  
