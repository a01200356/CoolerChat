package chat;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

import p2p.ConnectionsHandler;
import p2p.Message;

/**
 * CoolerChat command line interface.
 * @author Marc
 */
public class CoolerChat {
	private CoolerChat() {}
	
	/**
	 * Run chat in command line.
	 * @param args Optional --debug flag to print verbose output.
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String line;
		
		// Greeting text.
		System.out.println("Welcome to CoolerChat.\nTo exit the application, type ADIOS.\nThis chat is not meant to provide secure communications. Use at your own discretion.\n");
		
		// Read args if given.
		if (args.length > 0) {
			if (args[0].equals("--debug")) {
				ConnectionsHandler.setDebugMode(true);
				System.out.println("Debug mode is on.\n");
			}
		}
		
		// Prompt remote IP address.
		System.out.println("To connect to an existing network type the IP address of the remote peer.");
		System.out.println("To start a new network, leave IP field blank.");
		System.out.print("Remote peer's IP address: ");
		line = sc.nextLine();
		line = line.trim();
		
		// If remote peer's IP is given, try to connect to p2p network through it.
		if (line.length() > 0) {
			try {
				ConnectionsHandler.findNetwork(InetAddress.getByName(line));
			} catch (UnknownHostException e) {
				System.err.println("Could not connect to peer in " + line + ": " + e.getMessage());
				System.out.println("Adiós.");
				System.exit(1);
			}
		}

		// Create connection listener for incoming messages from other peers.
		if (!ConnectionsHandler.listen()) {
			System.err.println("Adiós.");
			System.exit(1);
		}
		
		// Listen to user input and send each line as a message.
		while (sc.hasNextLine()) {
			line = sc.nextLine();

			// Exit chat if user types exit command.
			if (line.trim().equals("ADIOS")) {
				ConnectionsHandler.quitNetwork();
				break;
			}
			
			// Broadcast message unless it's empty.
			if (!line.trim().isEmpty()) {
				Message msg = new Message(line);
				ConnectionsHandler.broadcastMessage(msg);
			}
		}
		sc.close();
		System.exit(0);
	}
}