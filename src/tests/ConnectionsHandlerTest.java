package tests;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.*;

import p2p.ConnectionsHandler;
import p2p.Peer;

public class ConnectionsHandlerTest {

	/**
	 * Assert that local peer always refers to the same object.
	 * @throws UnknownHostException
	 */
	@Test
	public final void testGetLocalPeer() throws UnknownHostException {
		// Local peer should never be null.
		Peer local = ConnectionsHandler.getLocalPeer();
		assertNotNull(local);
		
		// It should always return the same peer.
		assertEquals(local, ConnectionsHandler.getLocalPeer());
		
		// Modifications on local peer should apply globally.
		local.setIpAddress(InetAddress.getByName("1.1.1.1"));
		assertNotNull(ConnectionsHandler.getLocalPeer().getUUID());
		assertEquals(local, ConnectionsHandler.getLocalPeer());
		assertEquals(local.getUUID(), ConnectionsHandler.getLocalPeer().getUUID());
	}

}
