package tests;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import org.junit.*;

import com.google.gson.Gson;

import p2p.Peer;

public class PeerTest {

	private Peer peer;
	
	@Before
	public final void beforeTest() {
		peer = new Peer();
	}
	
	@After
	public final void afterTest() {
		peer = null;
	}
	
	/**
	 * Test basic constructor.
	 */
	@Test
	public final void testPeer() {
		assertNull(peer.getUUID());
		assertNull(peer.getIpAddress());
	}
	
	/**
	 * UUID should automatically be set if IP address changes.
	 * The same UUID should be generated given the same IP address.
	 * @throws UnknownHostException
	 */
	@Test
	public final void testSetIpAddress() throws UnknownHostException {
		Peer peerA = new Peer();
		peerA.setIpAddress(InetAddress.getByName("1.1.1.1"));
		
		Peer peerB = new Peer();
		peerB.setIpAddress(InetAddress.getByName("1.1.1.1"));
		
		Peer peerC = new Peer();
		peerC.setIpAddress(InetAddress.getByName("2.2.2.2"));
		
		assertEquals(peerA.getUUID(), peerB.getUUID());
		assertNotEquals(peerA.getUUID(), peerC.getUUID());
	}
	
	/**
	 * Test if two peers are the same.
	 * @throws UnknownHostException
	 */
	@Test
	public final void testIsSame() throws UnknownHostException {
		Peer peerA = new Peer();
		peerA.setIpAddress(InetAddress.getByName("1.1.1.1"));
		assertFalse(peer.isSame(peerA));
		
		peer.setIpAddress(InetAddress.getByName("1.1.1.1"));
		assertTrue(peer.isSame(peerA));
		
		peer.setIpAddress(InetAddress.getByName("2.2.2.2"));
		assertFalse(peer.isSame(peerA));
	}
	
	/**
	 * Neighbour peer should only be added once.
	 * @throws UnknownHostException
	 */
	@Test
	public final void testAddPeer() throws UnknownHostException {
		addNeighbour();
		assertEquals(peer.getNeighboursSize(), 1);
		addNeighbour();
		assertEquals(peer.getNeighboursSize(), 1);
	}

	/**
	 * Peer should be removed.
	 * Removing already removed peer should do nothing.
	 * @throws UnknownHostException
	 */
	@Test
	public final void testRemovePeer() throws UnknownHostException {
		UUID neighbourUUID = addNeighbour();
		
		peer.removePeer(neighbourUUID);
		assertEquals(peer.getNeighboursSize(), 0);
		
		peer.removePeer(neighbourUUID);
		assertEquals(peer.getNeighboursSize(), 0);
	}

	/**
	 * Get neighbours as JSON string.
	 */
	@Test
	public final void testGetNeighboursJSON() {
		UUID neighbourUUID = addNeighbour();
		String jsonString = peer.getNeighboursJSON();
		
		assertEquals((new Gson()).fromJson(jsonString, Peer[].class).length, 1);
		assertEquals((new Gson()).fromJson(jsonString, Peer[].class)[0].getUUID(), neighbourUUID);
	}

	/**
	 * Parsed JSON should be the same as generated JSON if they are based on the same list.
	 * Parsing JSON should not be possible if peer has no UUID.
	 * @throws UnknownHostException
	 */
	@Test
	public final void testParseNeighboursJSON() throws UnknownHostException {
		// Generate jsonString from local peer's list and then wipe list.
		UUID neighbourUUID = addNeighbour();
		String jsonString = peer.getNeighboursJSON();
		peer.removePeer(neighbourUUID);
		
		// Try to parse the generated jsonString.
		// Peers on the JSON should be added to neighbours.
		peer.parseNeighboursJSON(jsonString);
		assertEquals(peer.getNeighboursSize(), 1);
		
		// Compare two jsonStrings to ensure they haven't changed.
		String jsonString2 = peer.getNeighboursJSON();
		assertEquals(jsonString, jsonString2);
		
		// Try parse rubbish. Assert that nothing changed.
		int neighboursSize = peer.getNeighboursSize();
		peer.parseNeighboursJSON("rubbish");
		assertEquals(neighboursSize, peer.getNeighboursSize());
	}

	/**
	 * NOT a test, but an auxiliary function.
	 * @return Added UUID.
	 */
	private final UUID addNeighbour() {
		Peer neighbour;
		try {
			neighbour = new Peer(InetAddress.getByName("1.1.1.1"));
		} catch (Exception e) {
			neighbour = null;
		}
		peer.addPeer(neighbour);
		
		return neighbour.getUUID();
	}
}
