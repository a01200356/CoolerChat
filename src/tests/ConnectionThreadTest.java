package tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import org.junit.*;

import p2p.ConnectionThread;
import p2p.ConnectionsHandler;
import p2p.CoolerProtocol;
import p2p.Message;
import p2p.Peer;

public class ConnectionThreadTest {

	ConnectionThread thread;
	InetAddress localIP, remoteIP;
	
	@Before
	public final void beforeTest() throws UnknownHostException {
		localIP = InetAddress.getByName("127.0.0.1");
		remoteIP = InetAddress.getByName("4.3.2.1");
	}
	
	@After
	public final void afterTest() {
		thread = null;
	}
	
	/**
	 * Validate that correct response is generated from received request according to CoolerChat protocol.
	 */
	@Test
	public final void testParseRequest() {
		thread = new ConnectionThread(new Peer(remoteIP), null);
		
		// Bad inputs.
		assertEquals(thread.parseRequest(null), "");
		assertEquals(thread.parseRequest(""), "");
		assertEquals(thread.parseRequest("x"), "");
		
		// HI
		// Respond by greeting remote peer.
		assertEquals(thread.parseRequest(CoolerProtocol.HI + localIP.toString()), CoolerProtocol.HI + remoteIP.toString());
		assertEquals(ConnectionsHandler.getLocalPeer().getIpAddress(), localIP);
		
		// GETPEERS
		// Respond with JSONified peer list.
		assertEquals(thread.parseRequest(String.valueOf(CoolerProtocol.GETPEERS)), CoolerProtocol.PEERLIST + ConnectionsHandler.getLocalPeer().getNeighboursJSON());
		
		// PEERLIST
		// Prepare peer list.
		Peer auxPeer = new Peer(remoteIP);
		ConnectionsHandler.getLocalPeer().addPeer(auxPeer);
		String peerList = ConnectionsHandler.getLocalPeer().getNeighboursJSON();
		// Remove auxPeer to empty local peer's neighbours list.
		ConnectionsHandler.getLocalPeer().removePeer(auxPeer.getUUID());
		
		// Assert that parsed peer list is added to local peer's neighbours.
		assertEquals(thread.parseRequest(CoolerProtocol.PEERLIST + peerList), "");
		assertEquals(peerList, ConnectionsHandler.getLocalPeer().getNeighboursJSON());
		
		// BYE
		// Assert that peer is removed.
		int sizeBeforeRemoval = ConnectionsHandler.getLocalPeer().getNeighboursSize();
		assertEquals(thread.parseRequest(String.valueOf(CoolerProtocol.BYE)), "");
		assertEquals(ConnectionsHandler.getLocalPeer().getNeighboursSize(), sizeBeforeRemoval-1);
		
		// MESSAGE
		// Create message from remote peer, parse it and assert that author is added as neighbour.
		Message msg = new Message(new Peer(remoteIP), new Date(), "Hola mundo.");
		int sizeBeforeAddition = ConnectionsHandler.getLocalPeer().getNeighboursSize();
		assertEquals(thread.parseRequest(CoolerProtocol.MESSAGE + msg.toJSON()), "");
		assertEquals(ConnectionsHandler.getLocalPeer().getNeighboursSize(), sizeBeforeAddition+1);
		
		// Redirect stdout to ByteArrayOutputStream and compare output to parsed message.
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		assertEquals(thread.parseRequest(CoolerProtocol.MESSAGE + msg.toJSON()), "");
		assertEquals(out.toString(), "\n" + msg + "\n");
	}

	/**
	 * Validate that correct post is built according to CoolerChat protocol.
	 */
	@Test
	public final void testBuildRequest() {
		// Invalid header.
		thread = new ConnectionThread(new Peer(remoteIP), 'x');
		assertEquals(thread.buildRequest(), "");
		
		// HI
		// Assert that greeting has remote IP address.
		thread = new ConnectionThread(new Peer(remoteIP), CoolerProtocol.HI);
		assertEquals(thread.buildRequest(), CoolerProtocol.HI + remoteIP.toString());
		
		// MESSAGE
		// Assert that post contains the actual message.
		Message msg = new Message("Hola mundo.");
		thread = new ConnectionThread(new Peer(remoteIP), msg);
		assertEquals(thread.buildRequest(), CoolerProtocol.MESSAGE + msg.toJSON());
		
		// GETPEERS
		thread = new ConnectionThread(new Peer(remoteIP), CoolerProtocol.GETPEERS);
		assertEquals(thread.buildRequest(), String.valueOf(CoolerProtocol.GETPEERS));
		
		// BYE
		thread = new ConnectionThread(new Peer(remoteIP), CoolerProtocol.BYE);
		assertEquals(thread.buildRequest(), String.valueOf(CoolerProtocol.BYE));
	}

}
