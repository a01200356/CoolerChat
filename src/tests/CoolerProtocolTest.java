package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import p2p.CoolerProtocol;

public class CoolerProtocolTest {

	/**
	 * Check that protocol correctly tells which types of request need a response and which ones don't.
	 */
	@Test
	public final void testNeedsResponse() {
		// Require response.
		assertTrue(CoolerProtocol.needsResponse(CoolerProtocol.HI));
		assertTrue(CoolerProtocol.needsResponse(CoolerProtocol.GETPEERS));
		
		// Don't require response.
		assertFalse(CoolerProtocol.needsResponse(CoolerProtocol.PEERLIST));
		assertFalse(CoolerProtocol.needsResponse(CoolerProtocol.MESSAGE));
		assertFalse(CoolerProtocol.needsResponse(CoolerProtocol.BYE));
		
		// Non protocol header.
		assertFalse(CoolerProtocol.needsResponse('x'));
	}

}
