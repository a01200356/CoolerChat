package tests;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import org.junit.*;

import p2p.Message;
import p2p.Peer;

public class MessageTest {

	private Message msg;
	
	@Before
	public final void beforeTest() {
		Peer author;
		try {
			author = new Peer(InetAddress.getByName("1.1.1.1"));
		} catch (UnknownHostException e) {
			author = null;
		}
		Date date = new Date();
		msg = new Message(author, date, "Hi.");
	}
	
	@After
	public final void afterTest() {
		msg = null;
	}
	
	/**
	 * Test that Message can convert back and forth to JSON string.
	 */
	@Test
	public final void testToAndFromJSON() {
		// Assert that Message is unchanged when serialized to JSON back and forth.
		String jsonString = msg.toJSON();
		Message msg2 = Message.fromJSON(jsonString);
		assertNotNull(msg2);
		assertEquals(jsonString, msg2.toJSON());
		assertEquals(msg.getAuthor().getUUID(), msg2.getAuthor().getUUID());
		assertEquals(msg.toString(), msg2.toString());
		
		// Try to parse empty or malformed objects.
		assertEquals(Message.fromJSON("{}"), null);
		assertEquals(Message.fromJSON("rubbish"), null);
	}

}
