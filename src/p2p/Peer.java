package p2p;

import java.net.InetAddress;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.Gson;

/**
 * A Peer represents a running instance (i.e. peer) of the CoolerChat p2p network.
 * @author Marc
 */
public class Peer {
	private UUID uuid;  // The UUID is hashed from the public IP address.
	private InetAddress ipAddress;  // Public IP address that this peer listens from.
	private transient ConcurrentHashMap<UUID, Peer> neighbours;

	/**
	 * Constructor for local peer.
	 * Public IP address is not known yet.
	 */
	public Peer() {
		neighbours = new ConcurrentHashMap<UUID, Peer>();
	}
	
	/**
	 * Constructor for remote peer.
	 * @param ipAddress Peer's IP address.
	 */
	public Peer(InetAddress ipAddress) {
		this.neighbours = null;
		this.setIpAddress(ipAddress);
	}
	
	/**
	 * UUID is hashed from IP address.
	 * @return UUID
	 */
	public UUID getUUID() {
		return uuid;
	}

	public InetAddress getIpAddress() {
		return ipAddress;
	}
	
	/**
	 * IP address is expected to be unique in the network.
	 * Setting the IP address also changes the UUID of the peer (UUID is a hash of the IP address).
	 * @param ipAddress New IP address.
	 */
	public void setIpAddress(InetAddress ipAddress) {
		this.ipAddress = ipAddress;
		uuid = UUID.nameUUIDFromBytes(ipAddress.getAddress());
		if (neighbours != null) removePeer(uuid);  // In case peer has itself as neighbour.
	}
	
	public int getNeighboursSize() {
		return neighbours.size();
	}
	
	public ConcurrentHashMap<UUID, Peer> getNeighbours() {
		return neighbours;
	}
	
	/**
	 * Compares given peer with this peer based on UUIDs.
	 * @param peer Peer to compare this peer to.
	 * @return True if parameter peer is considered to be the same as this peer.
	 * If current peer has no UUID yet, it will always return false.
	 */
	public boolean isSame(Peer peer) {
		if (this.getUUID() == null) return false;
		if (peer.getUUID().compareTo(this.getUUID()) == 0) return true;
		return false;
	}
	
	/**
	 * Add a peer to neighbours list.
	 * @param peer Peer to be added.
	 */
	public void addPeer(Peer peer) {
		int sizeBeforeAddition = neighbours.size();
		
		// Add unless it's me.
		// If I still don't know who am I, add peers anyway.
		if (!isSame(peer) && peer.getUUID() != null) {
			neighbours.putIfAbsent(peer.getUUID(), peer);
			if (neighbours.size() > sizeBeforeAddition) {
				System.out.println("Added peer " + peer.getUUID());
			}
		}
	}
	
	/**
	 * Remove peer from neighbours list.
	 * @param uuid Peer to be removed.
	 */
	public void removePeer(UUID uuid) {
		int sizeBeforeRemoval = neighbours.size();
		
		neighbours.remove(uuid);
		
		if (sizeBeforeRemoval > neighbours.size()) {
			System.out.println("Removed " + uuid);
			if (neighbours.isEmpty()) {
				System.out.println("Everyone's gone. You are alone in this world.");
			}
		}
	}
	
	/**
	 * Get JSON list of all neighbours.
	 * @return JSONified neighbours list.
	 */
	public String getNeighboursJSON() {
		return (new Gson()).toJson(neighbours.values().toArray());
	}
	
	/**
	 * Parse JSON list of peers and update neighbours accordingly.
	 * @param jsonString JSONified neighbours list.
	 */
	public void parseNeighboursJSON(String jsonString) {
		Peer[] peerList;
		try {
			peerList = (new Gson()).fromJson(jsonString, Peer[].class);
		} catch (Exception e) {
			if (ConnectionsHandler.isDebugMode()) {
				System.err.println("Failed to parse peer list: " + jsonString);
			}
			return;
		}
		
		for (Peer neighbour : peerList) {
			addPeer(neighbour);
		}
	}
	
	/**
	 * Serialize to JSON string.
	 * @return JSONified peer.
	 */
	public String toJSON() {
		return (new Gson()).toJson(this);
	}
	
	/**
	 * Parse Peer from JSON string.
	 * @param jsonString JSONified peer.
	 * @return Peer contained in JSON.
	 */
	public static Peer fromJSON(String jsonString) {
		if (jsonString == "{}") return null;
		try {
			return (new Gson()).fromJson(jsonString, Peer.class);
		} catch (Exception e) {
			if (ConnectionsHandler.isDebugMode()) {
				System.err.println("Failed to parse peer: " + jsonString);
			}
			return null;
		}
	}
}