package p2p;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Thread that handles communication between a single pair of peers.
 * @author Marc
 */
public class ConnectionThread extends Thread {
	private Peer remotePeer;
	private Socket socket;
	private PrintWriter writer;
	private BufferedReader reader;
	private char protocolHeader;
	private Message message;
	
	/**
	 * Used when listening to remote peer.
	 * @param socket Socket already opened.
	 */
	public ConnectionThread(Socket socket) {
		this.socket = socket;
	}
	
	/**
	 * Used when connecting to remote peer.
	 * @param remotePeer Peer to connect to.
	 * @param protocolHeader Type of request to send using CoolerProtocol.
	 */
	public ConnectionThread(Peer remotePeer, char protocolHeader) {
		this.remotePeer = remotePeer;
		this.protocolHeader = protocolHeader;
	}
	
	/**
	 * Used when sending message to remote peer.
	 * @param remotePeer Peer to connect to.
	 * @param message Message to send.
	 */
	public ConnectionThread(Peer remotePeer, Message message) {
		this.remotePeer = remotePeer;
		this.message = message;
		this.protocolHeader = CoolerProtocol.MESSAGE;
	}
	
	/**
	 * Initialize socket (if not given) as well as reader and writer streams.
	 * @return True if initialization is successful.
	 */
	private boolean initializeConnection() {
		try {
			if (socket == null) {
				socket = new Socket();
				try {
					socket.connect(new InetSocketAddress(remotePeer.getIpAddress(), CoolerProtocol.getPort()), 5000);
				} catch(Exception e) {
					if (ConnectionsHandler.isDebugMode()) {
						System.err.println(e.getMessage());
					}
					closeSocket();
					return false;
				}
			}
			writer = new PrintWriter(this.socket.getOutputStream(), true);
			reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			return true;
			
		} catch (IOException e) {
			System.err.println(e.getMessage());
			closeSocket();
			ConnectionsHandler.getLocalPeer().removePeer(remotePeer.getUUID());
			return false;
		}
	}
	
	/**
	 * Safely close socket in case of error.
	 */
	private void closeSocket() {
		if (!socket.isClosed()) {
			try {
				socket.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
	/**
	 * Parse received request and generate its response.
	 * Local peer and neighbours list may be updated according to received data.
	 * @param request Received request from remote peer.
	 * @return Response to send to remote peer.
	 */
	public String parseRequest(String request) {
		if (request == null || request.isEmpty()) return "";
		ConnectionsHandler.getLocalPeer().addPeer(remotePeer);
		
		// Parse header.
		switch (request.charAt(0)) {
		
			case CoolerProtocol.HI:
				// If listener doesn't know its own IP address yet.
				if (ConnectionsHandler.getLocalPeer().getUUID() == null) {
					try {
						ConnectionsHandler.getLocalPeer().setIpAddress(InetAddress.getByName(request.substring(2)));
						if (ConnectionsHandler.isDebugMode()) {
							System.out.println("I am peer: " + ConnectionsHandler.getLocalPeer().toJSON());
						}
					} catch (UnknownHostException e) {
						if (ConnectionsHandler.isDebugMode()) {
							System.err.println(e.getMessage());
						}
						ConnectionsHandler.getLocalPeer().removePeer(remotePeer.getUUID());
					}
				}
				// Return greeting.
				return CoolerProtocol.HI + remotePeer.getIpAddress().toString();
				
			case CoolerProtocol.GETPEERS:
				return CoolerProtocol.PEERLIST + ConnectionsHandler.getLocalPeer().getNeighboursJSON();
				
			case CoolerProtocol.PEERLIST:
				ConnectionsHandler.getLocalPeer().parseNeighboursJSON(request.substring(1));
				break;
				
			case CoolerProtocol.MESSAGE:
				Message receivedMessage = Message.fromJSON(request.substring(1));
				if (receivedMessage != null) {
					System.out.println("\n" + receivedMessage);
				}
				break;
				
			case CoolerProtocol.BYE:
				ConnectionsHandler.getLocalPeer().removePeer(remotePeer.getUUID());
				System.out.println(remotePeer.getUUID() + " is gone.");
				break;
		}
		
		return "";
	}
	
	/**
	 * Build post that will be sent.
	 * @return Request to send to remote peer.
	 */
	public String buildRequest() {
		// Always start post with protocol header.
		String request = String.valueOf(protocolHeader);
		
		switch (protocolHeader) {
			case CoolerProtocol.HI:
				request += remotePeer.getIpAddress();
				break;
			case CoolerProtocol.GETPEERS:
				break;
			case CoolerProtocol.MESSAGE:
				request += message.toJSON();
				break;
			case CoolerProtocol.BYE:
				break;
			default:
				return "";  // Invalid protocol header.
		}
		
		return request;
	}
	
	/**
	 * Read line send from remote peer.
	 * @return Line read.
	 */
	private String readLine() {
		String line;
		try {
			line = reader.readLine();
		} catch (IOException e) {
			if (ConnectionsHandler.isDebugMode()) {
				System.err.println("Failed to read request from " + socket.getInetAddress() + ": " + e.getMessage());
			}
			closeSocket();
			return null;
		}
		
		return line;
	}
	
	/**
	 * Either send or receive post.
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		if (!initializeConnection()) {
			if (ConnectionsHandler.isDebugMode()) {
				System.err.println("Error: Could not connect with peer at " + socket.getInetAddress());
			}
			// Remove peer if not found.
			if (remotePeer != null) {
				ConnectionsHandler.getLocalPeer().removePeer(remotePeer.getUUID());
			}
			return;
		}
		
		String remotePost;
		String localPost;
		
		// If I'm listening.
		if (remotePeer == null) {
			remotePeer = new Peer(socket.getInetAddress());
			
			// Read and parse request.
			remotePost = readLine();
			if (ConnectionsHandler.isDebugMode()) {
				System.out.println(socket.getInetAddress() + ": " + remotePost);
			}
			localPost = parseRequest(remotePost);
			
			// If there's a response, send it.
			if (!localPost.isEmpty()) {
				if (ConnectionsHandler.isDebugMode()) {
					System.out.println("Local peer: " + localPost);
				}
				writer.println(localPost);
			}
			
		} else {
			// If I'm the one calling.
			localPost = buildRequest();
			if (localPost.isEmpty()) return;
			
			if (ConnectionsHandler.isDebugMode()) {
				System.out.println("Local peer: " + localPost);
			}
			writer.println(localPost);
			
			// Respond only if required by protocol.
			if (CoolerProtocol.needsResponse(protocolHeader)) {
				remotePost = readLine();
				if (ConnectionsHandler.isDebugMode()) {
					System.out.println(socket.getInetAddress() + ": " + remotePost);
				}
				parseRequest(remotePost);
			}
		}
		
		closeSocket();
	}
}
