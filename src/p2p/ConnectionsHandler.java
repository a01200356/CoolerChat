package p2p;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Centralized library to handle connections with p2p network.
 * @author Marc
 */
public class ConnectionsHandler {
	private static Peer localPeer;
	private static ListenerThread connectionsListener;
	private static boolean debugMode = false;

	private ConnectionsHandler() {}
	
	/**
	 * Gets or creates local peer.
	 * @return Peer object representing the locally running peer.
	 */
	public static Peer getLocalPeer() {
		if (ConnectionsHandler.localPeer == null) {
			ConnectionsHandler.localPeer = new Peer();
		}
		return ConnectionsHandler.localPeer;
	}
	
	/**
	 * Greet a peer from an existing CoolerChat network and get list of fellow peers.
	 * @param ipAddress Remote IP address to contact.
	 */
	public static void findNetwork(InetAddress ipAddress) {
		callPeer(new Peer(ipAddress), CoolerProtocol.HI);
		callPeer(new Peer(ipAddress), CoolerProtocol.GETPEERS);
	}
	
	/** 
	 * Open thread calling remotePeer with a protocolHeader type post.
	 * @param remotePeer Peer to call.
	 * @param protocolHeader Type of post to send according to CoolerProtocol.
	 */
	public static void callPeer(Peer remotePeer, char protocolHeader) {
		new ConnectionThread(remotePeer, protocolHeader).start();
	}
	
	/** 
	 * Send message to a single specific peer.
	 * @param remotePeer Peer to call.
	 * @param msg Message to send.
	 */
	public static void sendMessage(Peer remotePeer, Message msg) {
		new ConnectionThread(remotePeer, msg).start();
	}
	
	/**
	 * Send message to all local peer's neighbours.
	 * @param message Message to send.
	 */
	public static void broadcastMessage(Message message) {
		if (getLocalPeer().getNeighbours().isEmpty()) {
			System.out.println("Everyone's gone. You are alone in this world.");
		}
		for (Peer neighbour : getLocalPeer().getNeighbours().values()) {
			ConnectionsHandler.sendMessage(neighbour, message);
		}
	}
	
	/**
	 * Send BYE to all local peer's neighbours and close listening socket.
	 */
	public static void quitNetwork() {
		connectionsListener.closeSocket();
		
		for (Peer neighbour : getLocalPeer().getNeighbours().values()) {
			ConnectionsHandler.callPeer(neighbour, CoolerProtocol.BYE);
		}
		
		// Wait to give time for outgoing BYE posts to be sent.
		// TODO: Less hackish way to achieve this.
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			if (ConnectionsHandler.isDebugMode()) {
				System.err.println(e.getMessage());
			}
		}
	}
	
	/**
	 * Create and run ListenerThread.
	 * Returns true if listener is created successfully.
	 * Should only be called once.
	 */
	public static boolean listen() {
		if (connectionsListener == null) {
			
			try {
				connectionsListener = new ListenerThread();
			} catch (IOException e) {
				System.err.println("Failed to create ListenerThread: " + e.getMessage());
				return false;
			}
			
			connectionsListener.start();
		}
		
		return true;
	}
	
	/**
	 * @return If debug mode is on.
	 */
	public static boolean isDebugMode() {
		return debugMode;
	}
	
	/**
	 * Set debug mode for more verbose printing.
	 */
	public static void setDebugMode(boolean mode) {
		debugMode = mode;
	}
}