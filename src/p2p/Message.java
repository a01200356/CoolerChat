package p2p;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;

/**
 * Chat message to be sent or read.
 * Contains text only content.
 * @author Marc
 */
public class Message {
	private Peer author;
	private Date date;
	private String content;
	
	/**
	 * Create message.
	 * @param content Text content to be sent.
	 */
	public Message(String content) {
		author = ConnectionsHandler.getLocalPeer();
		date = new Date();
		this.content = content;
	}
	
	/**
	 * Parse message.
	 * @param author Peer that created the message.
	 * @param date Creation date of message.
	 * @param content Text content of message.
	 */
	public Message(Peer author, Date date, String content) {
		this.author = author;
		this.date = date;
		this.content = content;
	}
	
	public Peer getAuthor() {
		return this.author;
	}
	
	/**
	 * Serialize to JSON string.
	 * @return JSONified Message.
	 */
	public String toJSON() {
		return (new Gson()).toJson(this);
	}
	
	/**
	 * Parse message from JSON string.
	 * @param jsonString JSONified Message.
	 * @return Parsed message.
	 */
	public static Message fromJSON(String jsonString) {
		if (jsonString == "{}") return null;
		try {
			return (new Gson()).fromJson(jsonString, Message.class);
		} catch (Exception e) {
			if (ConnectionsHandler.isDebugMode()) {
				System.err.println("Failed to parse message: " + jsonString);
			}
			return null;
		}
	}

	/**
	 * Printable version of message to show user.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
			return (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).format(date)
					+ author.getUUID() + ":\n"
					+ content + "\n";
		} catch (NullPointerException e) {
			return super.toString();
		}
	}
}