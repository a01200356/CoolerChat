package p2p;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

/**
 * Server listener to handle incoming connections from other peers.
 * @author Marc
 */
public class ListenerThread extends Thread {

	private ServerSocket serverSocket;
	
	/**
	 * @throws IOException
	 */
	public ListenerThread() throws IOException {
		serverSocket = new ServerSocket();
		serverSocket.setReuseAddress(true);
		serverSocket.bind(new InetSocketAddress(CoolerProtocol.getPort()));
	}
	
	/**
	 * Permanently listens to all incoming requests.
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		while (!serverSocket.isClosed()) {
			try {
				new ConnectionThread(serverSocket.accept()).start();
			} catch (IOException e) {
				if (ConnectionsHandler.isDebugMode()) {
					System.err.println(e.getMessage());
				}
			}
		}
	}

	/**
	 * Safely close server socket.
	 */
	public void closeSocket() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			System.err.println("ServerThread failed to close ServerSocket: " + e.getMessage());
		}
	}
}
