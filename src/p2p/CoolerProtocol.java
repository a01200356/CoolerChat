package p2p;

/**
 * Library with CoolerChat protocol's definitions.
 * Used as headers to communicate between peers.
 * @author Marc
 */
public class CoolerProtocol {
	private static final int port = 1789;
	public static final char HI = 'H';  // Must contain the IP address of the peer being greeted.
	public static final char BYE = 'B';
	public static final char GETPEERS = 'G';
	public static final char PEERLIST = 'L';  // Must contain JSONified peer list.
	public static final char MESSAGE = 'M';  // Must contain JSONified message.
	
	/**
	 * @return Port number that needs to be opened to use p2p network.
	 */
	public static int getPort() {
		return port;
	}
	
	/**
	 * Tells if a post with parameter as protocol header requires a response according to CoolerChat protocol.
	 * @param protocolHeader Protocol header to evaluate.
	 * @return True if protocolHeader requires response.
	 */
	public static boolean needsResponse(char protocolHeader) {
		return protocolHeader == HI || protocolHeader == GETPEERS;
	}
	
	private CoolerProtocol() {}
}
